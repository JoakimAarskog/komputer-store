# Komputer-store
A website that allows the user to work and gain money, bank it, take a loan and ultimately buy a computer.

## Background
### Task

This project is made as an assignment for the Noroff Accelerate program.

### Requirements

- The work button must give the user 100 of a nondescript currency.
- The bank buttons transfers the money to the balance.
- The user can take a loan that is at maximum twice the amount of the current balance.
- If the user has a loan, 10% of the salary must go towards loan, the rest gets transferred to the balance.
- If the user has enough money in salary, the loan can be fully paid.
- There is also an option to buy a computer, this can be done if the user has enough money in the balance.

## Usage

In VSCode use the Extension Live Server, then right click and click "Open with Liver Server".
It's also possible to open the index.html file in a browser.

## Author

Joakim Aarskog https://gitlab.com/JoakimAarskog

## License
MIT © Joakim Aarskog
