// Getting the HTML elements
const computerOptionsEl = document.getElementById("laptopsDropdown");
const computerSpecs = document.getElementById("laptopFeatures");
const computerName = document.getElementById("laptopName");
const computerDesc = document.getElementById("laptopDesc");
const computerPrice = document.getElementById("laptopPrice");
const computerImage = document.getElementById("laptopImg");

const baseURL = "https://noroff-komputer-store-api.herokuapp.com/";

const btnGetALoan = document.getElementById("btnGetALoan");
const btnBank = document.getElementById("btnBank");
const btnWork = document.getElementById("btnWork");
const btnBuyNow = document.getElementById("btnBuyNow");
const btnPayLoan = document.getElementById("btnPayLoan");
const loansButtonsEL = document.getElementById("loansButtons");

const outstandingLoanEL = document.getElementById("outstandingLoan");
const balanceEl = document.getElementById("txtBalance");
const pay = document.getElementById("txtWork");
const currentLoanEl = document.getElementById("txtLoan");
const PAY_AMOUNT = 100;

// Declaring variables
let currentLoan = 0;
let currentBalance = 0;
let currentPay = 0;
let isLoan = false;


// Hardcoding the first select option for initial render
firstComputer = {
  id: 1,
  title: "Classic Notebook",
  description: "A little old, but turns on.",
  specs: [
    "Has a screen",
    "Keyboard works, mostly",
    "32MB Ram (Not upgradable)",
    "6GB Hard Disk",
    "Comes with Floppy Disk Reader (Free) - Requires cable",
    "Good excersice to carry",
  ],
  price: 200,
  stock: 1,
  active: true,
  image: "assets/images/1.png",
};

// Event listeners
// Adds an event listener to the Get a loan button
btnGetALoan.addEventListener("click", function () {
  let tempLoan = prompt("Take out a loan");
  loan = Number(tempLoan);
  if (loan !== null && loan != 0) {
    if (loan <= currentBalance * 2 && isLoan == false) {
      currentBalance = addMoney(currentBalance, loan);
      currentLoan = addMoney(currentLoan, loan);

      isLoan = true;

      disableButton(btnGetALoan);
      hideElement(btnGetALoan);
      showElement(btnPayLoan);
      setInnerText(balanceEl, currentBalance);
      showLoan(currentLoan);
    }
  }
});

btnWork.addEventListener("click", function () {
  currentPay = addMoney(currentPay, PAY_AMOUNT)
  setInnerText(pay, currentPay);
});

btnPayLoan.addEventListener("click", function () {
  if (currentPay >= currentLoan) {
    currentPay = reduceMoney(currentPay, currentLoan);
    currentLoan = 0;

    currentBalance = addMoney(currentBalance, currentPay);
    currentPay = 0;

    isLoan = false;

    setInnerText(balanceEl, currentBalance);
    setInnerText(pay, currentPay);

    showElement(btnGetALoan);
    enableButton(btnGetALoan);
    hideLoanElements();
  }
});

btnBank.addEventListener("click", function () {
  let tenPercent = 0;
  if (pay.innerText != 0) {
    if (currentLoan > 0) {
      tenPercent = currentPay * 0.1;
      currentLoan = reduceMoney(currentLoan, tenPercent);
      if (currentLoan < 0) currentLoan = 0;
    }

    currentBalance = addMoney(currentBalance, currentPay - tenPercent)
    currentPay = 0;

    setInnerText(pay, currentPay);
    setInnerText(balanceEl, currentBalance);

    if (currentLoan > 0) setInnerText(currentLoanEl, ` -${currentLoan}`);
    else {
      hideLoanElements();
      enableButton(btnGetALoan);
    }
  } else alert("You have nothing to transfer.");
});

btnBuyNow.addEventListener("click", function () {
  const tempPrice = Number(computerPrice.innerText);
  if (currentBalance >= tempPrice) {
    currentBalance = reduceMoney(currentBalance, tempPrice);
    setInnerText(balanceEl, currentBalance);
    alert(
      `Congratulation! You successfully bought the ${computerName.innerText}`
    );
  } else alert("You can't afford the computer! You have to work.");
});

// Adds an event listener on the select tag, calls on the handleComputerSelectionChange when a new computer is picked
computerOptionsEl.addEventListener("change", handleComputerSelectionChange);

// API stuff
// Fetching the computers from the API
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
  .then((response) => response.json())
  .then((data) => (computers = data))
  .then((computers) => addComputersToOption(computers));

// A function that iterates through the array of computers and calls on another function that adds a computer as an option on the select tag
function addComputersToOption(computers) {
  computers.forEach((computer) => addComputerToOption(computer));
  computerOptionsEl.options[0].selected = true;
}

// Adds a computer as an option on the select tag
function addComputerToOption(computer) {
  const computerEl = document.createElement("option");
  computerEl.value = computer.id;
  computerEl.appendChild(document.createTextNode(computer.title));
  computerOptionsEl.appendChild(computerEl);
}

// Handles the change and updates the text when the user picks a computer from the select list
function handleComputerSelectionChange(e) {
  const selectedComputer = computers[e.target.selectedIndex];
  setSelectedComputer(selectedComputer);
}

function setSelectedComputer(selectedComputer) {
  computerName.innerText = selectedComputer.title;
  computerDesc.innerText = selectedComputer.description;
  computerPrice.innerText = selectedComputer.price;
  computerDesc.innerText = selectedComputer.description;
  computerImage.src = baseURL + selectedComputer.image;
  computerSpecs.innerText = "";
  // Iterates though the array of specs and adds them as text with a new line for each
  selectedComputer.specs.forEach((spec) => {
    computerSpecs.innerText += spec + "\n";
  });
}

setSelectedComputer(firstComputer);

// Helper functions

function reduceMoney(target, amount) {
  return target - amount
}

function addMoney(target, amount) {
  return target + amount;
}

function showLoan(currentLoan) {
  showElement(outstandingLoanEL.childNodes[1]);
  showElement(outstandingLoanEL.childNodes[3]);

  currentLoanEl.innerText = `- ${currentLoan}`;
}

function hideLoan() {
  hideElement(outstandingLoanEL.childNodes[1]);
  hideElement(outstandingLoanEL.childNodes[3]);
}

function showElement(element) {
  element.classList.remove("hidden");
}

function hideElement(element) {
  element.classList.add("hidden");
}

function enableButton(button) {
  button.disabled = false;
}

function disableButton(button) {
  button.disabled = true;
}

function setInnerText(element, text) {
  element.innerText = text;
}

function hideLoanElements() {
  hideLoan();
  hideElement(btnPayLoan);
}

